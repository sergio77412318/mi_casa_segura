<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get ('/inicio', function(){
    return view('inicio');
});
Route::get('/ficha',function(){
    return view('ficha');
});
Route:: get ('/tramite',function(){
    return view('tramite');
});
Route::get('/rtecnica',function(){
    return view('rtecnica');
});
Route::get ('/rlegal',function(){
        return view('rlegal');
});
Route::get('/ruat',function(){
    return view('ruat');
});
Route::get('/ram',function(){
    return view('ram');
});
Route::get('/informes',function(){
    return view('informes');
});
Route::get('/retecnica',function(){
    return view('retecnica');
});
Route:: get ('/rtecniaobservaciones',function(){
    return view('rtecniaobservaciones');
});
Route::get('/relegal',function(){
    return view('relegal');
});
Route::get('/rlegalobservaciones',function(){
    return view('rlegalobservaciones');
});
Route::get('/reruat',function(){
    return view('reruat');
});
Route:: get ('/ruatobservaciones',function(){
        return view('ruatobservaciones');
});

